#!/usr/bin/python
import argparse
import os
import json

def main():
    parser = argparse.ArgumentParser(description='Zips cli json files together.', fromfile_prefix_chars='@')
    parser.add_argument('--directory','-d', help='Directory location', default="./cli/")
    parser.add_argument('--output','-o', help='Output filename', default="./cli.json")
    parser.set_defaults(func=default)
    parsed = parser.parse_args()
    parsed.func(vars(parsed))

def default(args):
    mypath = str(os.path.dirname(args['directory']))
    onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
    all_commands = []

    for file in onlyfiles:
        with open(os.path.join(mypath, file)) as json_data:
            d = json.load(json_data)
            for cmd in d['commands']:
                all_commands.append(cmd)

    new_dict= {'commands':all_commands}

    with open(args['output'], 'w') as outfile:
        json.dump(new_dict, outfile, indent=4)



if __name__ == '__main__':
    main()
