-module(rebar3_cli_plugin_prv).
-author("griffin.nozell@plexxi.com").
-export([init/1, do/1, format_error/1]).

-define(PROVIDER, cli).
-define(DEPS, [app_discovery]).

%% ===================================================================
%% Public API
%% ===================================================================
-spec init(rebar_state:t()) -> {ok, rebar_state:t()}.
init(State) ->
    Provider = providers:create([
				 {name, ?PROVIDER},            % The 'user friendly' name of the task
				 {module, ?MODULE},            % The module implementation of the task
				 {bare, true},                 % The task can be run by the user, always true
				 {deps, ?DEPS},                % The list of dependencies
				 {example, "rebar3 cli"}, % How to use the plugin
				 {short_desc, "A rebar plugin to handle generating code via the swagger script"},
				 {desc, "A rebar plugin to handle generating code via the swagger script"}
				]),
    {ok, rebar_state:add_provider(State, Provider)}.


-spec do(rebar_state:t()) -> {ok, rebar_state:t()} | {error, string()}.
do(State) ->
    %%io:format("~p, ~p~n", [rebar_state:get(State, base_dir, []), rebar_state:get(State, hooks, [])]),
    %% Generate (hard code for now but can read these atoms from opts specified in rebar.config
    generate(cli, State),
    {ok, State}.

-spec format_error(any()) ->  iolist().
format_error(Reason) ->
    io_lib:format("~p", [Reason]).

generate(cli, State) ->
  rebar_api:info("Generating CLI json object", []),
  Apps = rebar_state:all_plugin_deps(State),
  ThisApp=lists:foldl(fun(App, Acc) ->
      case rebar_app_info:name(App) of
          <<"rebar3_cli_plugin">> ->
        App;
          _ ->
        Acc
      end
    end, undefined, Apps),
  {ok, _}=rebar_utils:sh("python " ++ rebar_app_info:priv_dir(ThisApp) ++ "/cli_zipper.py --directory='./config/cli/' --output='./config/cli.json'",  [{use_stdout, false}, return_on_error]),
  %%{ok, _}=rebar_utils:sh("mv ./config/cli.json ./apps/cxcontrol/", [{use_stdout, false}, return_on_error]),
  ok.
